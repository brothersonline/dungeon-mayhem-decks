import React from 'react';
import {useTranslation} from "react-i18next";
import cards from "../cards.json";
import Card from "./Card";

function DeckView() {
  const { t } = useTranslation();

  let totalCardsCount = 0
  cards.forEach((card) => {
    totalCardsCount += card.amount
  })
  return (
    <>
      <section className="text-center">
        <h1>{t('cards')}</h1>
      </section>
      <section className="points text-center">
        <div className="container">
          <div className="row text-center animated fadeInUp">
            <div className="col-md-12">
              <h3>Bard</h3>
              <h3>Current number of cards: {totalCardsCount}</h3>
              {cards.map((card) => (
                <div className="col-md-3" key={card.id}>
                  <Card card={card}/>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default DeckView;
