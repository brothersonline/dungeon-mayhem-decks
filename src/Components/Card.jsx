import React from 'react';
import "./Card.css";
import LazyImage from "./LazyImage";
import {exportComponentAsJPEG} from "react-component-export-image";

const Card = ({card}) => {
  const cardImagePath = `${process.env.PUBLIC_URL}/img/characters/${card.id}.jpg`;
  const ref = React.createRef();

  return (
    <div>
      <div className={"card"} ref={ref}>
        <LazyImage className="card-image" unloadedSrc={`${process.env.PUBLIC_URL}/img/placeholder-image.png`} src={cardImagePath} alt="Game banner" />
        <div className={"abilities"}>
          {card.abilities.map((ability) => {
            const classIconPath = `${process.env.PUBLIC_URL}/img/abilityIcons/${ability}.png`;
            return(
              <div className="ability-icon" style={{
                background: `url(${classIconPath}) no-repeat center center`,
                backgroundSize: `cover`,
              }}>
              </div>
            )})}
        </div>
        {!card.description
          ? <div className={"card-title"}>{card.name}</div>
          : (
            <div className={"special-card-labels"}>
              <div className={"special-card-title"}>{card.name}</div>
              <div className={"special-card-description"}>{card.description}</div>
            </div>
          )
        }
      </div>
      <span className={"card-amount"}>Amount: {card.amount}</span>
      <button onClick={() => exportComponentAsJPEG(ref)}>
        Export As JPEG
      </button>
    </div>

  )}

export default Card;
