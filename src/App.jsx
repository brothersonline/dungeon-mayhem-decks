import React, { Suspense, useState } from "react";
import DeckView from "./Components/DeckView";
import './Components/i18n';
import i18next from "i18next";
import './App.css';

function App() {
  const [language, setLanguage] = useState('nl');

  const toggleLanguage = () => {
    const newLanguage = language === 'nl' ? 'en': 'nl';
    setLanguage(newLanguage);
    i18next.changeLanguage(newLanguage);
  }

  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        <button className="toggle-language-btn" onClick={() => toggleLanguage()}>{language}</button>
        <DeckView />
      </Suspense>
    </div>
  );
}

export default App;
